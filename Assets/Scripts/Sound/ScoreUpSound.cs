﻿using System;
using UnityEngine;

namespace Game
{
    public class ScoreUpSound : MonoBehaviour
    {
        [SerializeField] private AudioClip audioClip;

        private PlayerScoresPointEventChannel scoresPointEventChannel;
        private AudioSource audioSource;

        private void Awake()
        {
            scoresPointEventChannel = Finder.PlayerScoresPointEventChannel;
           gameObject.AddComponent<AudioSource>();
           audioSource = GetComponent<AudioSource>();
        }

        private void OnEnable()
        {
            scoresPointEventChannel.OnPointScored += PlayScoreSound;
        }

        private void OnDisable()
        {
            scoresPointEventChannel.OnPointScored -= PlayScoreSound;
        }

        private void PlayScoreSound()
        {
            audioSource.PlayOneShot(audioClip);
        }
    }
}