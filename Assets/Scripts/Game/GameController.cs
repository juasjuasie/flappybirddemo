﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    [RequireComponent(typeof(AudioSource))]
    public class GameController : MonoBehaviour
    {
        [SerializeField] private KeyCode startGameKey = KeyCode.Space;

        private PlayerDeathEventChannel playerDeathEventChannel;

        private PlayerScoresPointEventChannel playerScoresPointEventChannel;

        private GameState gamestate = GameState.MainMenu;

        private int score;
        public int Score => score;

        public event GameScoreChangedEventHandler OnGameScoreChanged;
        public event GameStateChangedEventHandler OnGameStateChanged;

        public GameState GameState
        {
            get { return gamestate; }
            private set
            {

                if (OnGameStateChanged != null)
                {
                    gamestate = value;
                    NotifyGaMeStateChanged();
                }
            }
        }

        private void NotifyGaMeStateChanged()
        {
            if (OnGameStateChanged != null) OnGameStateChanged(gamestate);
        }

        private Rigidbody2D rigidbody2D = null;

        private void Start()
        {

            if (!SceneManager.GetSceneByName("Game").isLoaded)
                SceneManager.LoadSceneAsync("Game", LoadSceneMode.Additive);
            else
            {
                SceneManager.SetActiveScene(SceneManager.GetSceneByName(Scenes.GAME));
            }
            

        }

        private IEnumerator LoadGame()
        {
            //Load screen
            yield return SceneManager.LoadSceneAsync("Game", LoadSceneMode.Additive);
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(Scenes.GAME));
            //Remove Canvas
        }

        private void Awake()
        {
            rigidbody2D = GetComponent<Rigidbody2D>();
            playerDeathEventChannel = Finder.PlayerDeathEventChannel;
            playerScoresPointEventChannel = Finder.PlayerScoresPointEventChannel;
            score = 0;
        }

        private void Update()
        {
            if (Input.GetKeyDown(startGameKey) && GameState == GameState.MainMenu)
                GameState = GameState.Playing;
            if (gamestate == GameState.GameOver && Input.GetKeyDown(startGameKey))
            {
                RestartGame();
            }
        }

        private void RestartGame()
        {
            GameState = GameState.MainMenu;
            score = 0;
            StartCoroutine(ReloadGame());
        }
        

        private IEnumerator UnloadGame()
        {
            yield return SceneManager.UnloadSceneAsync(Scenes.GAME);
        }

        private IEnumerator ReloadGame()
        {
            yield return LoadGame();
            yield return UnloadGame();
        }

    private void OnEnable()
        {
            playerDeathEventChannel.OnPlayerDeath += EndGame;
            playerScoresPointEventChannel.OnPointScored += PointScore;
        }

        public void PointScore()
        {
            score ++;
            Debug.Log("new Score:" + score);
            if (OnGameScoreChanged != null) OnGameScoreChanged(score);
        }
        
        private void OnDisable()
        {
            playerDeathEventChannel.OnPlayerDeath -= EndGame;
            playerScoresPointEventChannel.OnPointScored -= PointScore;
        }

        private void EndGame()
        {
            GameState = GameState.GameOver;
        }
        public delegate void GameStateChangedEventHandler(GameState newGameState);

        public delegate void GameScoreChangedEventHandler(int newScore);
    }
    public enum GameState
    {
        MainMenu,
        Playing,
        GameOver
    }
}