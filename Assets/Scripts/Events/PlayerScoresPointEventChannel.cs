﻿using UnityEngine;

namespace Game
{
    public class PlayerScoresPointEventChannel : MonoBehaviour
    {
        public event PlayerScoresEventHandler OnPointScored;
        
        public void NotifyPlayerScore()
        {
            if (OnPointScored != null)
            {
                OnPointScored();
            }
        }
        public delegate void PlayerScoresEventHandler();
    }
}