﻿using System;
using System.Numerics;
using Game;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

namespace Game
{
    [RequireComponent(typeof(PlayerMovement))]
    public class Player : MonoBehaviour
    {
        
        [SerializeField] private float targetMenuHeight = 0f;
        private PlayerMovement playerMovement;
        private GameController gameController;
        private PlayerDeathEventChannel playerDeathEventChannel;
        private PlayerScoresPointEventChannel playerScoresPointEventChannel;
        private Sensor sensor;
        
       

        private void Awake()
        {
            gameController = Finder.GameController;
            playerDeathEventChannel = Finder.PlayerDeathEventChannel;
            playerScoresPointEventChannel = Finder.PlayerScoresPointEventChannel;
            playerMovement = GetComponent<PlayerMovement>();
            sensor = GetComponentInChildren<Sensor>();
            


        }
        
        private void OnEnable()
        {
             sensor.OnHit += OnHit;
        }

        private void OnDisable()
        {
            sensor.OnHit -= OnHit;
        }

        private void OnHit(GameObject other)
        {
            Debug.Log("A Hit!");
            if (other.CompareTag("Death")) Die();
            else if (other.CompareTag("Point"))
            {
                IncreasePoint();
            }
        }

        private void IncreasePoint()
        {
            playerScoresPointEventChannel.NotifyPlayerScore();
            Debug.Log("Score increased");
            
        }

        [ContextMenu("Die")]
        private void Die()
        {
            Destroy(gameObject);
            playerDeathEventChannel.NotifyPlayerDeath();
        }
        private void Update()
        {
            //Appell à chauqe frame.
            //transform.Translate(Vector3.right * 5 * Time.deltaTime);
            var gameState = gameController.GameState;
            if (gameState == GameState.MainMenu)
            {
                if (transform.position.y <= targetMenuHeight)
                {
                   playerMovement.Flap();
                }
            }

            else if (gameState ==GameState.Playing)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                    playerMovement.Flap();
            }
            
        }
    }
}