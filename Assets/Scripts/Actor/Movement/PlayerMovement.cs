﻿using System;
using UnityEngine;

namespace Game
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private Vector2 flapForce = Vector2.up * 5;
        private Rigidbody2D rigidBody2D;

        private void Awake()
        {
            rigidBody2D = GetComponent<Rigidbody2D>();
        }

        public void Flap()
        {
            rigidBody2D.velocity = flapForce;
        }
    }
}