﻿using System;
using UnityEngine;

namespace Game.MovingObject
{

    public class InfiniteMovingLoop : MonoBehaviour
    {
        [Header("Visuals")] [SerializeField]  private Sprite sprite = null;
        [Header("behavior")] [Range(0,100)] [SerializeField] private int movingSpeed = 3;
        [Range(0, 10)] [SerializeField] private uint nbtiles = 3;
        [SerializeField] private string sortingLayerName = "Default";
        private Vector2 tileSize;
        private Vector3 initialPosition;
        private float offset;
        private void Awake(){
            //Lorsque composant est crée
        }

        private void Start()
        {
            tileSize = sprite.bounds.size;
            for (int i = 0; i < nbtiles; i++)
            {
                var tile = new GameObject(i.ToString());
                //Assigne un parent à GameObject
                tile.transform.parent = transform;
                // localPosition est la position par rapport au parent 
                // 
                tile.transform.localPosition = tileSize.x * i * Vector3.right;
                var spriteRenderer = tile.AddComponent<SpriteRenderer>();
                spriteRenderer.sprite = sprite;
                spriteRenderer.sortingLayerName = sortingLayerName;
            }
            //première frame du composant utilisé
            

            initialPosition = transform.position;
            offset = 0f;



        }

        private void OnEnable()
        {
            //Composant activé
            
        }

        private void OnDisable()
        {
            //Aprés activation, il est 
        }

        private void Update()
        {
            offset = ( offset + (movingSpeed * Time.deltaTime)) % tileSize.x;
            transform.position = initialPosition + Vector3.left * offset;
            //Appell à chauqe frame.
           // transform.Translate(Vector3.left * movingSpeed * Time.deltaTime);
            //if (transform.position.x <= -range )
            //{
              //  var position = transform.position;
                //position.x = range;
                //transform.position = position;
            //}
        }
#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            var size = sprite.bounds.size == null? Vector3.one : sprite.bounds.size;
            var center = transform.position;
            Gizmos.color = Color.blue;
            Gizmos.DrawWireCube(center, size);
            
        }
#endif

        private void FixedUpdate()
        {
            //Apellé en intervale lorsque l'engin de physique se met à jour.
        }

        private void OnDestroy()
        {
            //Apellé lorsque la composant est detruit.
            //LLe gameobject est detruit
        }
    
    }
}