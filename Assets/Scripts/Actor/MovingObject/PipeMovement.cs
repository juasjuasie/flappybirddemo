﻿using UnityEngine;

namespace Game
{
    public class PipeMovement : MonoBehaviour
    {
        
        [SerializeField] private Vector3 velocity = Vector3.left;
        private void Awake(){
            //Lorsque composant est crée
        }

        public void Move()
        {
            transform.Translate(velocity* Time.deltaTime);
        }
        
        private void Start()
        {
            
        }
        
    }
}