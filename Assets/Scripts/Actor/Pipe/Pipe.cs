﻿
using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;


namespace Game
{
    [RequireComponent(typeof(PipeMovement))]
    public class Pipe : MonoBehaviour
    {
        [Header("Behaviour")] [SerializeField] private float delayinSeconds = 10f;
        [Header("Position")] [SerializeField] private float minY = -1f;
        [SerializeField] private float maxY = 1f;
        
        private GameController gameController;
        private PipeMovement mover;
        
        private void Awake()
        {
            gameController = Finder.GameController;
            mover = GetComponent<PipeMovement>();
        }

        private void OnEnable()
        {
            StartCoroutine(DestroyPipe());
        }

        private IEnumerator DestroyPipe()
        {
            yield return new WaitForSeconds(delayinSeconds);
            Destroy(gameObject);
        }
        private void Start()
        {
            transform.Translate(Vector3.up* Random.Range(minY,maxY));
        }

        private void Update()
        {
            if (gameController.GameState == GameState.Playing)
            {
                mover.Move();
            }
        }
    }
    
    
}