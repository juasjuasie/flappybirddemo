﻿using Game;
using UnityEngine;

namespace Game
{
    public static class Finder
    {
        private const string GAME_CONTROLER_TAG = "GameController";

        private static PlayerDeathEventChannel playerDeathEventChannel;

        private static PlayerScoresPointEventChannel playerScoresPointEventChannel;
        
        private static GameController gameController;
        public static GameController GameController
        {
            get
            {
                if (gameController == null)
                {
                    gameController = GameObject.FindWithTag(GAME_CONTROLER_TAG).GetComponent<GameController>();
                }
                return gameController;

            }
            
        }
        public static PlayerDeathEventChannel PlayerDeathEventChannel
        {
            get
            {
                if (playerDeathEventChannel == null)
                {
                    playerDeathEventChannel = GameObject.FindWithTag(GAME_CONTROLER_TAG).GetComponent<PlayerDeathEventChannel>();
                }
                return playerDeathEventChannel;

            }
        }
        public static PlayerScoresPointEventChannel PlayerScoresPointEventChannel
        {
            get
            {
                if (playerScoresPointEventChannel == null)
                {
                    playerScoresPointEventChannel = GameObject.FindWithTag(GAME_CONTROLER_TAG).GetComponent<PlayerScoresPointEventChannel>();
                }
                return playerScoresPointEventChannel;
            }
        }
    }
}