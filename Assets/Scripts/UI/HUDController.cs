﻿using System;
using Game;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

namespace UI
{
    public class HUDController  : MonoBehaviour
    {
        private Canvas canvas;
        private GameController gameController;
        private Text text;

        private void Awake()
        {

            gameController = Finder.GameController;
            canvas = GetComponent<Canvas>();
            text = GetComponentInChildren<Text>();
        }
    

        private void Start()
        {
            UpdateMainMenuVisibility(gameController.GameState);
            updateUI(gameController.Score);
        }

        private void UpdateMainMenuVisibility(GameState gameState)
        {
            canvas.enabled = gameState == GameState.Playing;
        } 

        private void Update()
        {
            
        }

        private void updateUI( int score)
        {
            text.text = score.ToString("00");
        }

        private void OnEnable()
        {
            gameController.OnGameStateChanged += UpdateMainMenuVisibility;
            gameController.OnGameScoreChanged += updateUI;
        }
        
        private void OnDisable()
        {
            gameController.OnGameStateChanged -= UpdateMainMenuVisibility;
            gameController.OnGameScoreChanged -= updateUI;
        }
    }
}