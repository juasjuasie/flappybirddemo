﻿using System;
using Game;
using UnityEngine;

namespace UI
{
    public class MainMenuControler : MonoBehaviour
    {
        private Canvas canvas;
        private GameController gameController;

        private void Awake()
        {

            gameController = Finder.GameController;
            canvas = GetComponent<Canvas>();
        }
    

        private void Start()
        {
            UpdateMainMenuVisibility(gameController.GameState);
        }

        private void UpdateMainMenuVisibility(GameState gameState)
        {
            canvas.enabled = gameState == GameState.MainMenu;
        }

        private void OnEnable()
        {
            gameController.OnGameStateChanged += UpdateMainMenuVisibility;
        }
        
        private void OnDisable()
        {
            gameController.OnGameStateChanged -= UpdateMainMenuVisibility;
        }
    
    }
}