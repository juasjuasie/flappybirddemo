﻿using Game;
using UnityEngine;

namespace UI
{
    public class gameOverMenucontroller : MonoBehaviour
    {
        private Canvas canvas;
        private GameController gameController;

        private void Awake()
        {

            gameController = Finder.GameController;
            canvas = GetComponent<Canvas>();
        }
    

        private void Start()
        {
            UpdateMainMenuVisibility(gameController.GameState);
        }

        private void UpdateMainMenuVisibility(GameState gameState)
        {
            canvas.enabled = gameState == GameState.GameOver;
        }

        private void OnEnable()
        {
            gameController.OnGameStateChanged += UpdateMainMenuVisibility;
        }
        
        private void OnDisable()
        {
            gameController.OnGameStateChanged -= UpdateMainMenuVisibility;
        }
    }
}