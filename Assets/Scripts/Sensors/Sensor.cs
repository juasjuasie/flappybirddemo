﻿using System;
using UnityEngine;

namespace Game
{
    public class Sensor : MonoBehaviour
    {
        public event SensorEventhandler OnHit;
        private void OnTriggerEnter2D(Collider2D other)
        {
            var stimuli = other.gameObject.GetComponent<Stimuli>();
            if (stimuli != null)
            {
                var parent = other.transform.parent;
                var gameobject = parent != null ? parent.gameObject : other.gameObject;
                if (OnHit != null)
                {
                    OnHit(gameobject);
                }
            }
        }

        public delegate void SensorEventhandler(GameObject other);
    
    }
}